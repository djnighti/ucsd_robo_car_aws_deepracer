import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import cv2

CAMERA_NODE_NAME = 'camera_server'
CAMERA_TOPIC_NAME = '/camera_pkg/display_mjpeg'
FREQUENCY = 10  # hz
PERIOD = 1 / FREQUENCY


class ImageSubscriber(Node):
    def __init__(self):
        super().__init__(CAMERA_NODE_NAME)
        self.camera_subscriber = self.create_subscription(Image, CAMERA_TOPIC_NAME, self.live_calibration_values, 10)
        self.camera_subscriber
        self.bridge = CvBridge()

    def live_calibration_values(self, data):
        current_frame = self.bridge.imgmsg_to_cv2(data)
        cv2.imshow("camera", current_frame)
        cv2.waitKey(1)


def main(args=None):
    rclpy.init(args=args)
    image_subscriber = ImageSubscriber()
    rclpy.spin(image_subscriber)
    image_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
