# **ucsd_robo_car_aws_deepracer**

<img src="ros_ucsd_aws_logos.png">

### A simple ROS2 package using OpenCV on aws deepracer rc car with ackerman steering that can follow a line or stay between two lines.

<div align="center">

#### AWS Deepracer ROS2 Demo
[![AWS Deepracer ROS2 Demo](https://j.gifs.com/QkngMZ.gif)](https://youtu.be/7_inO2fhBq4)

</div>

<div>

## **Table of Contents**

  - [**Environment Configuration**](#environment-configuration)
    - [Create a gitlab account](#1-create-a-gitlab-account)
    - [Access this repository](#2-access-this-repository)
    - [Enable X11 forwarding](#enable-x11-forwarding)
  - [**Work Flow To Use This Repository**](#work-flow-to-use-this-repository)
  - [**Nodes**](#nodes)
    - [deepracer_calibration_node](#deepracer_calibration_node)
    - [lane_detection_node](#lane_detection_node)
    - [lane_guidance_node](#lane_guidance_node)
  - [**Topics**](#topics)
    - [ctrl_pkg/servo_msg](#ctrl_pkg-servo_msg)
    - [camera_pkg/display_mjpeg](#camera_pkg-display_mjpeg)
    - [centroid](#centroid)
  - [**Services**](#Services)
    - [servo_pkg/set_led_state](#servo_pkg-set_led_state)
  - [**Launch**](#launch)
    - [aws_rosracer.launch](#aws_rosracerlaunch)
  - [**Troubleshooting**](#troubleshooting)
    - [Camera not working](#camera-not-working)
    - [Throttle and steering not working](#throttle-and-steering-not-working)
    - [Deepracer wont boot or cannot SSH](#deepracer-wont-boot-or-cannot-ssh)
  - [**Demonstration videos**](#demonstration-videos)
    - [Number of lines to detect](#number-of-lines-to-detect)
    - [Error threshold](#error-threshold)
    - [Blue color detection](#blue-color-detection)
    - [Yellow color detection and line width specification](#yellow-color-detection-and-line-width-specification)
    - [Throttle and steering](#throttle-and-steering)
    - [Manipulating image dimensions](#manipulating-image-dimensions)
    - [Lane detection example with yellow filter](#lane-detection-example-with-yellow-filter)

<div align="center">

## **Environment Configuration**

</div>

<div align="center">

**NOTE: all text that are in boxes like `this` are to be entered into the _terminal window_**

</div>

#### **1. Create a gitlab account**

<div align="center">

**NOTE: This step is optional/recommended**

</div>

  a. Follow steps found <a href="https://about.gitlab.com/free-trial/" >here</a>

#### **2. Access this repository**

<div align="center">

  **NOTE: (if step 1 was not completed, skip steps 2.a-c and choose option 2 under step 2d**

</div>

  **a. Generate an SSH key and provide it to Gitlab for access to repositories**

   `ssh-keygen # Use all defaults`

  **b. Then press enter until you get to an empty comand line, then**

   `cat $HOME/.ssh/id_rsa.pub`

  **c. Then copy the ssh key and go back to Gitlab. Click on your user profile at the top right corner of the screen then**
     
  **click on _preferences_ from the drop down menu. Now a new panel on the left hand side of the screen wil apear, click on _SSH Keys_,**
     
  **then paste your SSH key into the text field and submit it.**

  **d. Create ROS workspace and obtain copy of ucsd_robo_car_simple_ros repository**
   
   `cd`

   `mkdir projects && cd projects`

   `mkdir deepracer_ws && cd deepracer_ws`

   `mkdir src && cd src`

  **e. Clone this repository with either of the following options:**

   option 1
   `git clone git@gitlab.com:djnighti/ucsd_robo_car_aws_deepracer.git`

   or (if you have not setup your own gitlab account and an ssh key)

   option 2
   `git clone https://gitlab.com/djnighti/ucsd_robo_car_aws_deepracer.git`

  **f. Add some lines of code to the bash script so that every time a new terminal is opened, root user is activated, builds this ROS2 package and sources all the correct ROS2 setups/variables and the pre-built deepracer ROS2 library**

  First for the **deepracer** user bashrc, enter the following into the terminal:
   
   `nano ~/.bashrc`
  
  add the following line of code at the end of the bash script as the **deepracer** user

   `sudo su`

  Then press 
   
   **ctrl-x** 
   
   Then press 
   
   **y**  (yes) 
   
   and then press 
   
   **enter** 
   
   to save an quit

  Now open a new terminal or restart current terminal

  Now the **root** user bashrc, enter the following into the terminal:

   `cd`

   `nano ~/.bashrc`

   add the following lines of code at the end of the bash script as the **root** user

   `cd`

   `xauth merge /home/deepracer/.Xauthority` (This is for ssh tunneling cv2 windows)

   `source /opt/ros/foxy/setup.bash`

   `source /opt/intel/openvino_2021/bin/setupvars.sh`

   `cd /home/deepracer/projects/deepracer_ws`

   `colcon build`

   `source install/setup.bash`

   `source /opt/aws/deepracer/lib/setup.bash`

   Then press 
   
   **ctrl-x** 
   
   Then press 
   
   **y**  (yes) 
   
   and then press 
   
   **enter** 
   
   to save an quit

  Now open a new terminal or restart current terminal to begin! Proceed to the following section to see the [**Work Flow To Use This Repository**](#work-flow-to-use-this-repository)

  **i. (ONLY DO THIS AS NEEDED) Now as this remote repository is updated, enter the following commands to update the local repository on the deepracer:**
   
   `cd`

   `cd projects/deepracer_ws/src/ucsd_robo_car_aws_deepracer`
   
   `git stash`

   `git pull`

   `chmod -R 777 .`

</div>

<div align="center">

## **Enable X11 forwarding**

</div>

Associated file: **x11_forwarding_steps.txt**

Some deepracers did not have this enabled, so if needed please read the steps in this file to setup X11 forwarding

<div align="center">

## **Work Flow To Use This Repository**

</div>

<div align="center">

  **NOTE: To exit any of the programs, in the terminal press the following 2 keys:**

  **ctrl-c**

</div>

1. Calibrate the camera, throttle and steering values using the [**deepracer_calibration_node**](#deepracer_calibration_node)

`ros2 run ucsd_robo_car_aws_deepracer deepracer_calibration_node`

2. Rebuild the package and source all ROS2 files!

      - **option 1 (faster):** open new ssh connection because all the commands for this should be in the bash script already
  
      - **option 2:** Enter the following commands in the same terminal that the [**deepracer_calibration_node**](#deepracer_calibration_node) was executed in

      

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`source /opt/ros/foxy/setup.bash`

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`source /opt/intel/openvino_2021/bin/setupvars.sh`

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`source /opt/aws/deepracer/lib/setup.bash`

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`colcon build`

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`source install/setup.bash`


</div>

3. Launch the aws racer 

`ros2 launch ucsd_robo_car_aws_deepracer aws_rosracer.launch.py`

4. Tune parameters in step 1 until desired behvior is achieved

<div align="center">

## **Nodes**

</div>


#### **deepracer_calibration_node**

Associated file: **deepracer_calibration_node.py**

Associated Topics:
- Subscribes to the [**camera_pkg/display_mjpeg**](#camera_pkg-display_mjpeg) topic
- Publishes to the [**ctrl_pkg/servo_msg**](#ctrl_pkg-servo_msg) topic

Associated Services:
- Calls to the [**servo_pkg/set_led_state**](#servo_pkg-set_led_state) service

See the [**lane_detection_node**](#lane_detection_node) for the explaination of the image processing.

Calibrate the camera, throttle and steering using by using the sliders to find:
- the right color filter 
- desired image dimmensions
- throttle values for both the optimal condtion (error = 0) and the non optimal condtion (error !=0) AKA go fast when error=0 and go slow if error !=0
- steering sensitivty change the Kp value to adjust the steering sensitivty (as Kp --> 1 steering more responsive, as Kp --> 0  steering less responsive) 

Below is a table of all the properties the operator can modify on the deepracer.

| Property   | Info |
| ----------  | --------------------- |
| lowH, highH | Setting low and high values for Hue  | 
| lowS, highS | Setting low and high values for Saturation | 
| lowV, highV | Setting low and high values for Value | 
| Inverted_filter | Specify to create an inverted color tracker | 
| min_width, max_width | Specify the width range of the line to be detected  | 
| number_of_lines | Specify the number of lines to be detected  | 
| error_threshold | Specify the acceptable error the robot will consider as approximately "no error" | 
| frame_width | Specify the width of image frame (horizontal cropping) | 
| rows_to_watch | Specify the number of rows (in pixels) to watch (vertical cropping) | 
| rows_offset | Specify the offset of the rows to watch (vertical pan) | 
| Steering_sensitivity | Specify the proportional gain of the steering | 
| Steering_value | Specify the steering value | 
| Throttle_mode | Toggle this slider at the end of calibration to the following 2 modes. |
| Throttle_mode 1 | zero_error_throttle_mode (find value for car to move when there is **no error** in steering)
| Throttle_mode 2 | error_throttle_mode(find value for car to move when there is **some error** in steering)| 
| Throttle_value | Specify the throttle value to be set in each of the throttle modes| 

These values are saved automatically to a configuration file, so just press control-c when the deepracer is calibrated. Below is the command to begin the calibration process.

`ros2 run ucsd_robo_car_aws_deepracer deepracer_calibration_node`

#### **lane_detection_node**

Associated file: **lane_detection.py**

Associated Topics:
- Subscribes to the [**camera_pkg/display_mjpeg**](#camera_pkg-display_mjpeg) topic
- Publishes to the [**centroid**](#centroid) topic

Associated Services:
- Calls to the [**servo_pkg/set_led_state**](#servo_pkg-set_led_state) service

This node uses opencv to identify line information from an image and will calculate an error with respect to the center axis of the onbaord camera.

Below are the descriptions of the different states the deepracer can be in which will illuminate the LED on the deepracer to quickly show the operator what decisions the deepracer is making.

| State | Description |
| ------ | ------ |
| straight curve | LED color: RED |
| curvy road | LED color: Green |
| STANDBY mode | LED color: Yellow |

Below is the color scheme for the openCV bounding boxes and their centroids.

| State | Description |
| ------ | ------ |
| 2 or more contours | green bounding boxes and a blue centroid to identify where the deepracer will steer |
| 1 contour | green bounding box with a red centroid to identify where the deepracer will steer |

#### **lane_guidance_node**

Associated file: lane_guidance.py

Associated Topics:
- Subscribes to the [**centroid**](#centroid) topic
- Publishes to the [**ctrl_pkg/servo_msg**](#ctrl_pkg-servo_msg) topic

Throttle and steering are based on the **calculated error** in the [**lane_detection_node**](#lane_detection_node) and the **error threshold** defined by the operator in the [**deepracer_calibration_node**](#deepracer_calibration_node). The steering system uses a proportional controller to have a more stable response.

<div align="center">

## **Topics**

</div>

#### **ctrl_pkg / servo_msg**
| Name       | Msg Type              | Info                                                       |
| ---------- | --------------------- | ---------------------------------------------------------- |
| /ctrl_pkg/servo_msg   | deepracer_interfaces_pkg.msg.ServoCtrlMsg  | Two Float values each from -1 to 1 for controlling throttle and steering | 

Useful command to stop vehicle:

`ros2 topic pub ctrl_pkg/servo_msg deepracer_interfaces_pkg/msg/ServoCtrlMsg "{angle: 0.0, throttle: 0.0}"`

#### **camera_pkg / display_mjpeg**
| Name       | Msg Type              | Info                                                       |
| ---------- | --------------------- | ---------------------------------------------------------- |
| /camera_pkg/display_mjpeg | sensor_msgs.msg.Image | Image last read from USB camera image                      |

Useful command to troubleshoot camera:

`ros2 topic echo /camera_pkg/display_mjpeg`

#### **centroid**
| Name       | Msg Type              | Info                                                       |
| ---------- | --------------------- | ---------------------------------------------------------- |
| /centroid   | std_msgs.msg.Float32    | Float value for that represents the error of the x coordinate of centroid in camera image space |

Useful command to troublshoot image processing:

`ros2 topic echo /centroid`

<div align="center">

## **Services**

</div>

#### **servo_pkg / set_led_state**

| Name | Srv Type | Info |
| ------ | ------ | ------ |
| servo_pkg/set_led_state | deepracer_interfaces_pkg.srv.SetLedCtrlSrv | An array of 3 integer values that define the values of red, green and blue |



<div align="center">

## **Launch**

</div>

#### **aws_rosracer.launch**

Associated file: aws_rosracer.launch.py

This file will launch [**lane_detection_node**](#lane_detection_node) and [**lane_guidance_node**](#lane_guidance_node) as well as load all the ROS-parameters selected in [**deepracer_calibration_node**](#deepracer_calibration_node)

**Before launching, please calibrate the deepracer first while on the test stand! See** [**deepracer_calibration_node**](#deepracer_calibration_node)

`ros2 launch ucsd_robo_car_aws_deepracer aws_rosracer.launch.py`

<div align="center">

## **Troubleshooting** 

<a href="https://aws.amazon.com/deepracer/getting-started/" >This link</a> provides some instructional videos provided by amazon to setup the deepracer

</div>

#### **Camera not working** 

If while running [**deepracer_calibration_node**](#deepracer_calibration_node) or [**aws_rosracer.launch**](#aws_rosracerlaunch) and if the cv2 windows do not open, then follow the procedure below to potentially resolve the issue.

1. Make sure camera is plugged in all the way into its USB socket
1. See if image feed is coming through on web browser. (Enter car IP address in browser URL and login to aws deepracer homepage)
1. Check to see if the camera topic is publishing data `ros2 topic echo /camera_pkg/display_mjpeg`
1. Restart the deepracer core service `systemctl restart deepracer-core.service`
1. Restart ROS2 daemon  `ros2 daemon stop` then `ros2 daemon start`
1. Reboot deepracer if none of the above worked and try again `sudo reboot now`

If the camera is still not working after trying the procedure above, then it could be a hardware issue. (Did the car crash?)

#### **Throttle and steering not working** 

If while running [**deepracer_calibration_node**](#deepracer_calibration_node) or [**aws_rosracer.launch**](#aws_rosracerlaunch) and the throttle and steering are unresponsive, then follow the procedure below to potentially resolve the issue.

1. Make sure ESC is turned on
1. Make sure battery has a charge (can verify by logining in to aws deepracer homepage in the web browser)
1. Make sure battery is plugged into deepracer compute module
1. Make sure servo and ESC wires are plugged into the deepracer compute module correctly
1. Check to see if the servo topic is publishing data 
1. Verify that the throttle values found in [**deepracer_calibration_node**](#deepracer_calibration_node) were loaded properly when running  [**aws_rosracer.launch**](#aws_rosracerlaunch) (Values will be printed to the terminal first when running the launch file) 
1. Restart the deepracer core service `systemctl restart deepracer-core.service`
1. Restart ROS2 daemon  `ros2 daemon stop` then `ros2 daemon start`
1. Reboot deepracer if none of the above worked and try again `sudo reboot now`

If the Throttle and steering are still not working after trying the procedure above, then it could be a hardware issue. (Did the car crash?)


#### **Deepracer wont boot or cannot SSH**

If trying to SSH into deepracer and are prompted with a message "refused to connect" or if left hanging,  then follow the procedure below to potentially resolve the issue.

1. Verify car is turned on (LED lights over power button should be blue but can be other colors)
1. Try to login through the web browser and enable SSH communication in the settings. (Enter car IP address in browser URL and login to aws deepracer homepage)
1. Plug in monitor, keyboard and mouse to see if you are brought to Ubuntu login screen, check ssh status and then enable SSH comminication in terminal `sudo systemctl status ssh` then `sudo ufw allow ssh`
1. Reboot deepracer if none of the above worked and try again `sudo reboot now`
1. Flash deepracer with latest image (if none of the above worked) with <a href="https://docs.aws.amazon.com/deepracer/latest/developerguide/deepracer-ubuntu-update-preparation.html" >these</a> instructions provided by amazon.

<div align="center">

## **Demonstration videos** 

</div>

<div align="center">

#### Number of lines to detect
[![Number of lines to detect](https://j.gifs.com/qQ7Lvk.gif)](https://youtu.be/5AVL68BTD0U)

</div>

<div align="center">

#### Error threshold
[![Error threshold](https://j.gifs.com/k28Xmv.gif)](https://youtu.be/ied1TDvpDK4)

</div>

<div align="center">

#### Yellow color detection
[![Yellow color detection](https://j.gifs.com/PjZoj6.gif)](https://youtu.be/c9rkRHGGea0)

</div>

<div align="center">

#### Blue color detection and line width specification
[![Blue color detection and line width specification](https://j.gifs.com/BrLJro.gif)](https://youtu.be/l2Ngtd461DY)

</div>

<div align="center">

#### Throttle and steering
[![Throttle and steering](https://j.gifs.com/362n6p.gif)](https://youtu.be/lhYzs5v6jtI)

</div>

<div align="center">

#### Manipulating image dimensions
[![Manipulating image dimensions](https://j.gifs.com/lR5oR1.gif)](https://youtu.be/_DhG6dduYPs)

</div>

<div align="center">

#### Lane detection example with yellow filter
[![lane detection example with yellow filter](https://j.gifs.com/6WRqXN.gif)](https://youtu.be/f4VrncQ7HU)

</div>


